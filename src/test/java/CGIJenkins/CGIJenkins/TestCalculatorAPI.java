package CGIJenkins.CGIJenkins;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.Assert;

public class TestCalculatorAPI {
	CalculatorAPI c;
	int Res;
@BeforeClass
public void startTest() {
	
	c=new CalculatorAPI();
}
@Test(priority=1)
public void TestwithPositiveNumbers() {
	Res=c.Addition(20,30);
	Assert.assertEquals(Res,50,"Postive numbers are not working" );
}

@Test(priority=2)
public void TestwithZeros() {
	Res=c.Addition(100,0);
	Assert.assertEquals(Res,100,"Addition function is not working with zero" );
}

@Test(priority=3)
public void TestwithMultiplicationPositiveNumbers() {
	Res=c.Multiplication(20,30);
	Assert.assertEquals(Res,600,"Multiplication Postive numbers are not working" );
}

@Test(priority=4)
public void TestMultiplicationwithZeros() {
	Res=c.Multiplication(100,0);
	Assert.assertEquals(Res,0,"Multiplication function is not working with zero" );
}

@Test(priority=5)
public void TestDivisionwithZeros() {
	Res=c.Division(10,5);
	Assert.assertEquals(Res,2,"Division function is not working with zero" );
}
}
